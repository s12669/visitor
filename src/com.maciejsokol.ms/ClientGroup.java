package com.maciejsokol.ms;

import java.util.ArrayList;
import java.util.List;

public class ClientGroup implements Visitable {
    private String memberName;

    public String getMemberName() {
        return memberName;
    }

    private static List<Client> clients = new ArrayList<Client>();

    public static List<Client> getClients() {
        return clients;
    }

    public void addClient(Client client) {
        clients.add(client);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return memberName;
    }
}
