package com.maciejsokol.ms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order implements Visitable {
    private String number;
    private double orderTotalPrice;
    private Date orderDate;
    private static List<Product> products = new ArrayList<Product>();

    public String getNumber() {
        return number;
    }

    public static List<Product> getProducts() {
        return products;
    }

    public double getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return number;
    }
}
