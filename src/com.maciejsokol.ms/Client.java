package com.maciejsokol.ms;

import java.util.ArrayList;
import java.util.List;

public class Client implements Visitable {
    private String number;
    private static List<Order> orders = new ArrayList<Order>();

    public String getNumber() {
        return number;
    }

    public static List<Order> getOrders() {
        return orders;
    }

    public void setNumber(String number) {

        this.number = number;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return number;
    }
}
