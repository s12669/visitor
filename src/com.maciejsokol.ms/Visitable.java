package com.maciejsokol.ms;

public interface Visitable {
    void accept(Visitor visitor);

    String giveReport();
}
