package com.maciejsokol.ms;

public class ReportMaker implements Visitor {

    private int numberOfClients;
    private int numberOfOrders;
    private int numberOfProducts;
    private String report;

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public int getNumberOfOrders() {
        return numberOfOrders;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public String showReport() {
        System.out.println("Clients list" + ClientGroup.getClients());
        System.out.println("Orders list" + Client.getOrders());
        System.out.println("Products list" + Order.getProducts());
        return report;
    }

    @Override
    public void visit(ClientGroup clientGroup) {
        System.out.println("Member name: " + clientGroup.getMemberName() + " " + clientGroup.getClients());
        numberOfClients++;
    }

    @Override
    public void visit(Client client) {
        System.out.println("Number: " + client.getNumber() + " " + client.getOrders());
        numberOfOrders++;
    }

    @Override
    public void visit(Order order) {

        System.out.println("Number: " + order.getNumber() + "; Total Price:  " + order.getOrderTotalPrice() + "; Order Date:  " + order.getOrderDate() + " " + order.getProducts());
        numberOfProducts++;
    }
}
