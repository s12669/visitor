package com.maciejsokol.ms;

public interface Visitor {
    void visit(ClientGroup clientGroup);

    void visit(Client client);

    void visit(Order order);
}
